---

- name: set vars to work with
  set_fact:
    wordpress_title: "{{ wordpresses[item][\"title\"] }}"
    wordpress_domain: "{{ wordpresses[item][\"domain\"] }}"
    wordpress_db_user_password: "{{ wordpresses[item][\"db_user_password\"] }}"
    wordpress_admin_password: "{{ wordpresses[item][\"admin_password\"] }}"
    wordpress_admin_email: "{{ wordpresses[item][\"admin_email\"] }}" 

- name: configure nginx virtual domain
  include: configure-nginx.yml
  with_items: "{{ wordpress_domain }}"

- name: Deploy php-fpm config
  template:
    src: templates/wordpress-php-fpm.conf.j2
    dest: '{{ php_fpm_config_path }}/pool.d/wordpress-php-fpm.conf'
    owner: root
    group: root
    mode: 0644

- name: Ensure writability of wordpress directory
  file:
    path: "/var/www/{{ wordpress_domain }}"
    state: directory
    owner: wordpress
    group: wordpress
    mode: 0775

- name: download wordpress with wp-cli
  become_user: wordpress
  command: wp-cli core download --path=/var/www/{{ wordpress_domain }}
  register: wp_download
  changed_when: "'WordPress files seem to already be present here.' not in wp_download.stdout"
  ignore_errors: yes

- name: create wordpress database
  mysql_db:
    name: "{{ item }}_db"
    state: present
    login_host: localhost
    login_user: root
    login_password: "{{ mysql_root_password }}"

- name: create wordpress database user
  mysql_user:
    name: "{{ item }}_user"
    password: "{{ wordpress_db_user_password }}"
    append_privs: yes
    priv: "{{ item }}_db.*:ALL"
    state: present
    login_host: localhost
    login_user: root
    login_password: "{{ mysql_root_password }}"

- name: create wp-config.php with wp-cli
  become_user: wordpress
  command: wp-cli core config --path=/var/www/"{{ wordpress_domain }}" --dbname="{{ item }}"_db --dbuser="{{ item }}"_user --dbpass="{{ wordpress_db_user_password }}"
  register: wp_config
  changed_when: "'file already exists.' not in wp_config.stdout"
  ignore_errors: yes

- name: install wordpress with wp-cli
  become_user: wordpress
  command: wp-cli core install --path=/var/www/"{{ wordpress_domain }}" --url="{{ wordpress_domain }}" --title="{{ wordpress_title }}" --admin_user="admin" --admin_password="{{ wordpress_admin_password }}" --admin_email="{{ wordpress_admin_email }}"
  register: wp_install
  changed_when: "'WordPress is already installed.' not in wp_install.stdout and 'The network already exists.' not in wp_install.stdout"
  ignore_errors: yes

- name: 'install plugin {{ item }}'
  become_user: wordpress
  command: 'wp-cli plugin install {{ item }} --activate --path=/var/www/{{ wordpress_domain }}'
  ignore_errors: yes
  with_items: '{{ wordpress_plugins_to_install }}'

- name: 'deactivate plugin {{ item }}'
  become_user: wordpress
  command: 'wp-cli plugin deactivate {{ item }} --path=/var/www/{{ wordpress_domain }}'
  ignore_errors: yes
  with_items: '{{ wordpress_plugins_to_deactivate }}'

- name: 'activate plugin {{ item }}'
  become_user: wordpress
  command: 'wp-cli plugin deactivate {{ item }} --path=/var/www/{{ wordpress_domain }}'
  ignore_errors: yes
  with_items: '{{ wordpress_plugins_to_activate }}'

- name: set permalink structure to postname
  become_user: wordpress
  command: 'wp-cli option update permalink_structure /%postname%/ --path=/var/www/{{ wordpress_domain }}'
  ignore_errors: yes

- name: add or remove cron job for wordpress plugin auto-upgrades
  cron:
    name: "{{ item }} wordpress plugin auto-upgrade"
    user: wordpress
    job: "/usr/local/bin/wp-cli plugin update --all --path=/var/www/{{ wordpress_domain }}"
    state: "{{ 'present' if wordpress_plugins_auto_upgrade else 'absent' }}"
    minute: "{{ 59|random }}"
    hour: "{{ [23]|random }}"

